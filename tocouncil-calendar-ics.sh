#!/bin/bash
# Generates a calendar in ICS format. Currently doesn't have all ICS features, be careful!

# Change output file name here:
outputfile="TOcouncil-calendar.ics"

bodyIds=$(curl -s 'https://secure.toronto.ca/council/api/multiple/decisionbody-list.json?termId=8' | jq -r '[.Records[].decisionBodyId] | @tsv')
meetingIds=$(for bodyId in $bodyIds ; do curl -s "https://secure.toronto.ca/council/api/multiple/meeting.json?decisionBodyId=$bodyId" | jq -r '.Records[].meetingId' ; done)

printf "BEGIN:VCALENDAR\nCALSCALE:GREGORIAN\nX-WR-CALNAME:TOcouncil Calendar\nX-WR-TIMEZONE:America/Toronto\nPRODID:codeberg.org/nev/tocouncil-calendar\nVERSION:2.0\n" > $outputfile

for meetingId in $meetingIds
do
	curl -s "https://secure.toronto.ca/council/api/individual/meeting/$meetingId.json" | jq -r '.Record.meeting |
	"BEGIN:VEVENT",
	"UID:\(.decisionBodyId)-\(.meetingId)-\(.meetingNumber)",
	"DTSTAMP;TZID=America/Toronto:\(now | strflocaltime("%Y%m%dT%H%M%S"))",
	"DTSTART;TZID=America/Toronto:\( .locationDates[0].startTime | tonumber/1000 | strflocaltime("%Y%m%dT%H%M%S") )",
	"DTEND;TZID=America/Toronto:\( .locationDates[0].endTime | tonumber/1000 | strflocaltime("%Y%m%dT%H%M%S") )",
	"LOCATION:\(.locationDates[0].location.locationName)",
	"SUMMARY:\(.decisionBody.decisionBodyName) Meeting #\(.meetingNumber) \(if .specialMeetingCd == "Y" then "(Special)" else "" end)",
	"DESCRIPTION:Agenda: https://secure.toronto.ca/council/#/committees/\(.decisionBodyId)/\(.meetingId)",
	"END:VEVENT"' >> $outputfile
done

printf "END:VCALENDAR" >> $outputfile

echo 'Done! 😎️'

# Example ICS formats for reference

# tmmis export
# BEGIN:VCALENDAR
# CALSCALE:GREGORIAN
# X-WR-CALNAME:Council and Committee Meeting Schedule Export
# X-WR-TIMEZONE:America/Toronto
# BEGIN:VEVENT
# DTSTART:20230824T093000
# DTEND:20230824T180000
# LOCATION:Committee Room 1 - City Hall
# SUMMARY:Executive Committee (Special)
# END:VEVENT
# END:VCALENDAR

# n8henrie
# BEGIN:VCALENDAR
# VERSION:2.0
# PRODID:n8henrie.com
# X-WR-CALNAME:Imported Calendar
# BEGIN:VEVENT
# SUMMARY:City Council Meeting #13
# DTSTART;VALUE=DATE-TIME:20231213T093000
# DTEND;VALUE=DATE-TIME:20231213T200000
# DTSTAMP;VALUE=DATE-TIME:20230822T183244Z
# UID:9035ef53-229b-46cf-93b5-59e4ba55450e___n8henrie.com
# DESCRIPTION:Agenda: https://secure.toronto.ca/council/#/committees/2462/23
#  200
# LOCATION:Council Chamber\, City Hall/Video Conference
# TRANSP:OPAQUE
# END:VEVENT

# gcal export
# BEGIN:VCALENDAR
# PRODID:-//Google Inc//Google Calendar 70.9054//EN
# VERSION:2.0
# CALSCALE:GREGORIAN
# METHOD:PUBLISH
# X-WR-CALNAME:TOcouncil
# X-WR-TIMEZONE:America/Toronto
# BEGIN:VEVENT
# DTSTART:20230726T180000Z
# DTEND:20230726T220000Z
# DTSTAMP:20230825T045709Z
# UID:CSVConvert002ab2dab69d4fb30cd8bdd4e2c6d39c
# CREATED:19000101T120000Z
# DESCRIPTION:Agenda: https://secure.toronto.ca/council/#/committees/2469/233
#  82
# LAST-MODIFIED:20230822T183844Z
# LOCATION:Video Conference
# SEQUENCE:0
# STATUS:CONFIRMED
# SUMMARY:Bid Award Panel Meeting #37
# TRANSP:OPAQUE
# END:VEVENT
