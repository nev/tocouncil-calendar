# tocouncil-calendar

Scripts to generate a calendar of Toronto City Council and committee meetings, using the TMMIS API. Very much a work in progress.

`tocouncil-calendar-csv.sh` produces a CSV of _all_ scheduled meetings, including past ones, that can be imported into Google Calendar. It is *static* and does not update if meetings are changed/added/cancelled, so also check the actual TMMIS calendar. Also, it prints out a crapton of stuff so I can see it's working, so if you don't want that, change ` | tee ` to ` > ` and ` | tee -a ` to ` >> `.

`tocouncil-calendar_current.csv.sh` is the same, except it only includes meetings in the future. 

`tocouncil-calendar-ics.sh` produces an ICS file that, in theory, can be updated regularly, publicly hosted, and subscribed to. However, I haven't quite figured out how to set it up so it nicely updates when meetings are changed/added/cancelled.

To do:

- [ ] combine scripts into one big one with selectable options
- [ ] add spinner/progress bar?
- [ ] make/host .ics version that can be updated & subscribed to
- [ ] figure out more efficient way to grab meeting data
- [ ] import events (CoA meetings, etc.) as well
- [ ] allow selecting date range
