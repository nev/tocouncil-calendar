#!/bin/bash
# This script creates a CSV file that can be imported into Google Calendar.

# Change output file name here:
outputfile="TOcouncil-calendar_current.csv"

echo 'Getting body IDs…'

# For different terms, change the number after "termID" here:
bodyIds=$(curl -s 'https://secure.toronto.ca/council/api/multiple/decisionbody-list.json?termId=8' | jq -r '[.Records[].decisionBodyId] | @tsv')

echo 'Getting meeting IDs…'

# Selects only meetings > the current date and time
# This throws an error but it seems to work anyway
meetingIds=$(for bodyId in $bodyIds
 	do curl -s "https://secure.toronto.ca/council/api/multiple/meeting.json?decisionBodyId=$bodyId" | jq -r '.Records[] | select((.meetingDate|tonumber/1000) >= now) | .meetingId'
	done)
	
echo 'Printing schedule…'

printf "Subject,Start Date,Start Time,End Date,End Time,All Day Event,Description,Location,Private\n" | tee $outputfile

for meetingId in $meetingIds
do
	curl -s "https://secure.toronto.ca/council/api/individual/meeting/$meetingId.json" | jq -r '.Record.meeting | ["\(.decisionBody.decisionBodyName) Meeting #\(.meetingNumber)", (.locationDates[0].startTime | tonumber/1000 | strflocaltime("%m/%d/%Y")), (.locationDates[0].startTime | tonumber/1000 | strflocaltime("%-I:%M %p")), (.locationDates[0].endTime | tonumber/1000 | strflocaltime("%m/%d/%Y")), (.locationDates[0].endTime | tonumber/1000 | strflocaltime("%-I:%M %p")), "False", "Agenda: https://secure.toronto.ca/council/#/committees/\(.decisionBodyId)/\(.meetingId)", .locationDates[0].location.locationName, "False"] | @csv'
done | tee -a $outputfile

echo 'Done! 😎️'
